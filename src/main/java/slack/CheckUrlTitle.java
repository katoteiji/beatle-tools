package slack;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CheckUrlTitle {
    public static void main(String[] argv) throws Exception {
        String title = CheckUrlTitle.getTitle(argv[0]);
        boolean result = CheckUrlTitle.judgeUrlTitle(title);
        if (!result) {
            System.out.println("ダメ");
        } else {
            System.out.println("OK");
        }
    }

    public static String getTitle(String pageUrl) throws Exception {
        URL url = new URL(pageUrl);
        URLConnection conn = url.openConnection();

        String charset = Arrays.asList(conn.getContentType().split(";")).get(1);
        String encoding = Arrays.asList(charset.split("=")).get(1);

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), encoding));
        StringBuffer response = new StringBuffer();
        String line;
        while ((line = in.readLine()) != null)
            response.append(line + "\n");
        in.close();

        Pattern titlePattern1 = Pattern.compile("<title>([^<]+)</title>", Pattern.CASE_INSENSITIVE);
        Matcher matcher1 = titlePattern1.matcher(response.toString());
        if (matcher1.find()) {
            return matcher1.group(1);
        }
        return null;
    }

    public static boolean judgeUrlTitle(String url) throws Exception {
        String objectUrl = url;
        String targetTitle = "仮想通貨ニュースサイト-CoinPost";
        int result = objectUrl.indexOf(targetTitle);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }
}
