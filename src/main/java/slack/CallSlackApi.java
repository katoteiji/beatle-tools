package slack;

import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;

public class CallSlackApi {
    public static void main(String[] args) {
        String title = null;
        try {
            title = CheckUrlTitle.getTitle("http://coinpost.jp/");
            boolean result = CheckUrlTitle.judgeUrlTitle(title);

            if (!result) {
                String message = "Alert: \"You could not get the title of the site.\"";
                SlackApi api = new SlackApi("https://hooks.slack.com/services/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");
                api.call(new SlackMessage("#server-monitoring", "Coinpost-Title-Checker", message));
            } else {
                System.out.println("OK");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
